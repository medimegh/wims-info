#!/bin/sh

CXXFLAGS="-std=c++11 -Wall -Wno-sign-compare -Wno-unused-value"

usage () {
    echo compile-and-run [program.cpp]
    echo
    echo Compiles and executes a c++ program.
    echo The standard input and output is passed down to the program.
    echo The exit status is that of the compiler.
}

if [ x"$1" = "x" ]; then
    usage
    exit 0
fi

ulimit -H -t 10
if g++ $CXXFLAGS $1; then
    # ulimit -H -t 1 # Apparently -t takes a number >= 10 for hard limit
    ulimit -t 1
   ./a.out
else
   exit $?
fi

