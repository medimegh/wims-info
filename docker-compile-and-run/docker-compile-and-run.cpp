#include <vector>
#include <iostream>
#include <cstdlib>
#include <sstream>
using namespace std;

#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>
#include <stdio.h>

/**
 * TODO:
 * - [X] make this into a program rather than a shell script for a minimum of safety
 * - [ ] set tight resources limits
 * - [ ] check proper handling of all potential errors
 * - [ ] make this into a plain C program to not add a dependency on C++
 **/

string compile_and_run = "compile-and-run.sh";
string bin_dir = "/home/wims/other/bin/";
//string bin_dir = "";
string docker = "/usr/bin/docker";
bool verbose = false;

void usage () {
    cerr << "docker-compile-and-run [program.cpp]" << endl;
    cerr << endl;
    cerr << "Compiles and executes a c++ program within a sandbox." << endl;
    cerr << "The standard input and output is passed down to the program." << endl;
    cerr << "The exit status is that of the compiler." << endl;
}

int my_exec(string cmd, const vector<string> args) {
    if (verbose) {
        cerr << "exec: " << cmd;
        for (unsigned int i=0; i<args.size(); i++)
            cerr << " " << args[i];
        cerr << endl;
    }
    vector<const char*> argv;
    argv.push_back(cmd.c_str());
    for (unsigned int i=0; i<args.size(); i++)
        argv.push_back(args[i].c_str());
    argv.push_back((char *) 0);
    const vector<const char *> argv2(argv.begin(), argv.end());
    return execv(cmd.c_str(), (char * const*)argv.data());
}

int my_system(string cmd, const vector<string> args, const string in, const string out, const string err) {
    // Taken from man waitpid
    int status;
    pid_t w;
    pid_t cpid = fork();
    if (cpid == -1) {
        perror("fork");
        exit(EXIT_FAILURE);
    }
    if (cpid == 0) {            /* Code executed by child */
        if ( in != "" ) {
            FILE* f = fopen(in.c_str(), "r");
            dup2(fileno(f), STDIN_FILENO);
            fclose(f);
        }
        if ( out != "" ) {
            FILE* f = fopen(out.c_str(), "w");
            dup2(fileno(f), STDOUT_FILENO);
            fclose(f);
        }
        my_exec(cmd, args);
        perror("exec");
        exit(EXIT_FAILURE);
    }
    w = waitpid(cpid, &status, 0);
    if (w == -1) {
        perror("waitpid");
        exit(EXIT_FAILURE);
    }
    return EXIT_SUCCESS;
}

int my_system(string cmd, const vector<string> args) {
    return my_system(cmd, args, "", "", "");
}

std::string my_popen(const string cmd, vector<string> args) {
    int pipefd[2];
    pid_t cpid;

    if (pipe(pipefd) == -1) {
        perror("pipe");
        exit(EXIT_FAILURE);
    }

    cpid = fork();
    if (cpid == -1) {
        perror("fork");
        exit(EXIT_FAILURE);
    }

    if (cpid == 0) {    /* Child writes to pipe */
        close(pipefd[0]);          /* Close unused read end */
        dup2(pipefd[1], STDOUT_FILENO);
        my_exec(cmd, args);
        perror("exec");
        exit(EXIT_FAILURE);
    }

    close(pipefd[1]);          /* Close unused write end */

    char buffer[128];
    std::string result = "";
    int n;
    while ((n = read(pipefd[0], buffer, sizeof(buffer))) > 0) {
        buffer[n] = 0;
        result += buffer;
    }
    wait(NULL);                /* Wait for child */
    return result;
}

string docker_run(string container, string cmd, vector<string> args) {
    // See http://docs.docker.com/engine/reference/run/#runtime-constraints-on-resources
    // for the resource limitations
    vector<string> docker_args = {"run",
                                  "--detach=true", // Run in background
                                  // With 10M, the compilation and run is much slower
                                  "--memory", "100M", "--memory-swap", "-1",
                                  // Not available with docker 1.7?
                                  // "--kernel-memory", "50M",
                                  // "--cpu-quota" "50000", // Allow for using max 50% of the CPU
                                  // Incompatible with detach
                                  // "--rm=true", // Automatically delete container when done with the command
                                  container, cmd};
    for (unsigned int i=0; i<args.size(); i++)
        docker_args.push_back(args[i]);
    string ID = my_popen(docker, docker_args);
    // see http://stackoverflow.com/questions/216823/whats-the-best-way-to-trim-stdstring
    ID.erase(ID.find_last_not_of(" \n\r\t")+1);
    return ID;
}

void docker_exec(string docker_id, vector<string> args) {
    vector<string> docker_args = {"exec", "-i", docker_id};
    for (unsigned int i=0; i<args.size(); i++)
        docker_args.push_back(args[i]);
    my_system(docker, docker_args);
}

void docker_cp(string docker_id, string source, string target) {
    // See http://stackoverflow.com/questions/22907231/copying-files-from-host-to-docker-container
    // TODO: replace with 'docker cp' of docker 1.8 when possible
    vector<string> docker_args = {"exec", "-i", docker_id, "/bin/bash",  "-c", "cat > "+target};
    my_system(docker, docker_args, source, "", "");
}

void docker_rm(string docker_id) {
    vector<string> docker_args = {"rm", "-f", docker_id};
    my_system(docker, docker_args, "", "/dev/null", "");
}

int main(int argc, char **argv) {
    //cout << my_popen("/usr/bin/id", {}) << "|" << endl;
    //printf("egid: %d\n", getegid());
    //exec("/usr/bin/id", {});
    //system("/usr/bin/id");

    if (argc != 2) {
        usage();
        return 0;
    }

    string program=argv[1];
    if (verbose)
        cerr << "Compiling and running: " << program << endl;
    string program_in_docker="prog.cpp";

    string docker_id = docker_run("crosbymichael/build-essential", "sleep", vector<string>({"10"}));
    if (verbose) {
        cerr << "docker_id: " << docker_id << endl;
        cerr << "Current directory: ";
        my_system("/bin/pwd", {});
    }
    docker_cp(docker_id, bin_dir+compile_and_run, compile_and_run);
    docker_cp(docker_id, program, program_in_docker);
    docker_exec(docker_id, vector<string>({ "chmod", "700", compile_and_run }));
    docker_exec(docker_id, vector<string>({ "./"+compile_and_run, program_in_docker}));
    docker_rm(docker_id);
    return 0;
}
