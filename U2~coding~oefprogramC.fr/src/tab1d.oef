\title{Tableau 1D}
\language{fr}
\author{Mathieu Clement-Ziza}
\email{clementm@necker.fr}

\text{C=tab1d}

\integer{niveau=2}
\integer{niveau=\confparm1==1?1}
\integer{niveau=\confparm1==2 or \confparm1==3?2}
\integer{niveau=\confparm1==4 or \confparm1==5?3}
\integer{niveau=\confparm1==6?4}

\text{var=shuffle(a,z,e,r,t,y,u,i,p,q,s,d,f,g,h,j,k,m,w,x,c,v,b,n)}
\text{var1=\var[1]}
\text{var2=\var[2]}
\text{var3=\var[3]}
\integer{TAILLE=random(10..50)}

<!-- Niveau 1 -->
\text{sens_boucle1=randitem(croissant,decroissant)}
\text{valeur_boucle1=randitem(croissant,decroissant)}
\integer{init_compteur=random(0..20)}

<!-- Niveau 2 -->
\text{parite=randitem(pair,impair)}
\integer{val=random(1..3)}

<!-- Niveau 3 -->
\integer{max=\TAILLE/2}

<!-- Question -->
\integer{q1=random(0..\TAILLE-2)}
\integer{q2=\q1+1}

<!-- ###################### Generation du CODE ################### -->

\text{compteur=\niveau==4?&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;\var3=\var3+1;
}

<!-- blocs -->

<!-- La boucle 1 est la boucle de remplissage initiale de tab --->

\text{code_boucle1=\sens_boucle1 issametext croissant?<br>    for(\var1=0; \var1&#60TAILLE ; \var1=\var1+1):<br>    for(\var1=TAILLE-1; \var1&#62=0 ; \var1=\var1-1)}
\if {\valeur_boucle1 issametext croissant}
{
\text{code_boucle1=\code_boucle1
    &#123;
        tab[\var1]=\var1;
\compteur    &#125;}
}{
\text{code_boucle1=\code_boucle1
    &#123;
        tab[\var1]=TAILLE-\var1-1;
\compteur    &#125;}
}

<!-- boucle 2 test pour niveau 3 -->
\text{code_boucle2=
<br><br>    for (\var1=1; \var1&#60TAILLE ; \var1=\var1+1)
    &#123;
        if (tab[\var1] &#62 \max)
        &#123;
        	tab[\var1]=tab[\var1-1];
        &#125;
        else if (\var1 &#62 \max)
        &#123;
            tab[\var1]=tab[\var1]+1;
        &#125;
\compteur    &#125;}
    
<!-- boucle 3 pair / impair -->
\if{\parite issametext pair}
{
\text{code_boucle3=
<br><br>    for (\var1=0; 2*\var1&#60TAILLE ; \var1=\var1+1)
    &#123;
        tab[2*\var1] = tab[2*\var1]+\val;
\compteur    &#125;}
}
{
\text{code_boucle3=
<br><br>    for (\var1=0; 2*\var1+1&#60TAILLE ; \var1=\var1+1)
    &#123;
        \var2=\var1;
        tab[2*\var2] = tab[2*\var2]+\val;
\compteur    &#125;}
}

\text{code=<pre>
#include&#60;stdio.h&#62;

int main(void)
&#123;
    #define TAILLE \TAILLE
    int tab[TAILLE];
    int \var1,\var2;
    int \var3=\init_compteur;
    \code_boucle1}

\if{\niveau>2}{
	\text{code=\code \code_boucle2}
}
\if{\niveau>1}{
	\text{code=\code \code_boucle3}
}

\text{code=\code <br>
    printf("Valeur de tab[\q1]" : %d\n, tab[\q1]);
    printf("Valeur de tab[\q2]" : %d\n, tab[\q2]);
    printf("Valeur de \var3" : %d\n, \var3);
    return 0;
&#125;
</pre>
}    

<!-- ################## Generation des resultats ################### -->

\matrix{results=wims( exec bioinfo \C \niveau \TAILLE \sens_boucle1 \valeur_boucle1 \parite \val \max \q1)}

\integer{res3=\results[3]+\init_compteur}

<!-- ############ Enonc� ############ -->

\statement{

<!-- Enonc�-->
<p>En analysant le programme suivant, d�terminez quelles seront les valeurs de \(tab [\q1]),
\(tab [\q2]) et de \(\var3)  affich� � l'�cran.

</p>
	<table border=0>
		<tr>
			<td>
				<table align="center" cellpadding=10 style="background-color:#F0FFFF;color:red;border:solid;">
					<tr>
					 <td>

					<!-- Affichage du Code C -->
					\special{expandlines \code}
					</td>
					</tr>
				</table>
			</td>
			<td>
				&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
			</td>
			<!-- Mis en forme html des reponse -->
			<td>
				<table align="center" border="1">
					<tr>
						<td>
							<i>Valeur de</i> \(tab [\q1]) :&nbsp;
						</td>
						<td>
							\embed{reply 1,7}
						</td>
					</tr>
					<tr>
						<td>
							<i>Valeur de</i> \(tab [\q2]) :
						</td>
						<td>
							\embed{reply 2,7}
						</td>
					</tr>
					<tr>
						<td>
							<i>Valeur de</i> \(\var3) :
						</td>
						<td>
							\embed{reply 3,7}
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>

}

\answer{ }{\results[1]}
\answer{ }{\results[2]}
\answer{ }{\res3}
