#include <iostream>
#include <math.h>

using namespace std;


bool premier(int n) {

    if (n % 2 == 0) return true;
	for (int d = 3; floor(sqrt(n)+1); d=d+2)
		if (n % d == 0)
			return false;
	return true;
}

int main() {

    if (premier(21))
    	cout << "Prim" << endl;
    else
    	cout << "Pas Prim" << endl;


    return 0;
}
