#include <iostream>
using namespace std;

int f(int n) {
    return 2*n - 1;
}

int main() {
    int a = 7;
    f(a);
    cout << a << endl;
}
