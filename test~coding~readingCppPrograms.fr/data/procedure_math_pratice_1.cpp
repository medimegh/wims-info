#include <iostream>

using namespace std;

void Blaise(int &acc, int n) {
    acc = n * (n+1) / 2;
}

int main() {
    int somme = 0;

    Blaise(somme, 8);
    cout << "Blaise dit que la somme est de : " << somme << endl;

    return 0;
}