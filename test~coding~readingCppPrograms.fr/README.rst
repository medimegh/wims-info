Module WIMS: Compr�hension de programmes C++
============================================

Pr�sentation
------------

Ce module pr�sente � l'�tudiant des programmes qu'il doit lire et
comprendre, afin de deviner quelle en sera la sortie, ou bien l'entr�e
appropri�e pour obtenir 42. Les programmes sont tir�s al�atoirement
d'une collection de programmes group�s par th�mes. Pour rajouter un
nouveau programme il suffit de l'ajouter dans le r�pertoire
idoine. Voir ci-dessous.

Pour l'instant, la collection est constitu�e d'exercices de C++,
donn�s dans le cadre des cours d'introduction � l'informatique `Info
111 <Nicolas.Thiery.name>`_ et Info 121 de la licence MPI de
l'Universit� Paris Sud, mais l'infrastructure est con�ue pour �tre
g�n�ralisable � d'autres langages de programmation.


�tendre le module avec de nouveaux programmes
---------------------------------------------

Le r�pertoire `data/`_ contient des programmes C++. Le nom des
programmes est de la forme ``<theme>_nom.cpp`` ou ``<theme>_nom_input.cpp``.

Dans le premier cas, l'utilisateur doit deviner la sortie du
programme. Dans le second cas, le programme doit lire un entier entre
0 et 99 sur l'entr�e standard, et l'utilisateur doit deviner lequel
donne 42; pour l'instant cet entier doit �tre unique.

En vue d'obtenir un peu de variabilit� les programmes sont transform�s
avant affichage et compilation, de la mani�re suivante:

- Remplacement de I1, I2, I3, I4 par i,j,k,n respectivement (ou une
  permutation de ces derniers);

- Remplacement de D1, D2, D3 par x,y,z respectivement (ou une
  permutation de ces derniers);

- Remplacement de F1 par f.

Ces remplacements sont fait de mani�re purement textuelle. Ainsi,
�AI1� sera remplac� par, par exemple, �Ai�.

Dans la version actuelle, le remplacement est fait statiquement avant
le chargement du module dans WIMS (�diter la premi�re ligne du
Makefile pour changer/compl�ter la liste des remplacements). � terme
ce remplacement sera fait dynamiquement par WIMS, avec une permutation
diff�rente � chaque ex�cution de l'exercice.

.. TODO:: Am�liorer l'ent�te du Makefile pour que ne soient remplac�s que les mots complets.

L'infrastructure du module est implant�e dans
`src/cpp/read_program.cpp`_.

.. WARNING::

    Dans l'�tat actuel de l'exercice, l'utilisateur ne peut pas
    rentrer de r�ponse vide. Il faut donc que tous les programmes du
    premier type affiche au moins un caract�re. �viter les caract�res
    unicode compliqu�s aussi :-)


Mise � jour de l'exercice sur WIMS
----------------------------------

Lancer `make archive` dans ce r�pertoire. Cela cr�� une archive:

    /tmp/modtool-test~coding~readingCppPrograms.fr.tgz

Que l'on peut charger dans son compte modtools sur sur WIMS avec:

    wims.u-psud.fr -> serveur de l'universit� -> Accueil WIMS -> modtools -> login -> Restauration



Documentation g�n�rique des modules WIMS
========================================

Pour installer des exercices OEF dans un module :

 1. Cr�er un module de type OEF

 2. Pour chaque exercice, cr�er dans le module un nouveau fichier 
    d'extension oef dans le r�pertoire src/ (exemple : src/euclide.oef). 
    Une zone de  texte appara�t ; y �crire (ou coller) le texte source 
    de l'exercice. Enregistrer les changements.

 3. Tester le module.

 4. Modifier � son go�t intro.phtml et endhook.phtml,
     et tester � nouveau.


%%%%%% Pour utiliser directement le module sur un serveur WIMS local,

1- Mettre  le template � la bonne place, 
en changeant le nom.

2. Modifier intro.phtml

3. Modifier le fichier INDEX.

4. Installer les fichiers sources.

5. Ex�cuter le script $wims_home/other/bin/src2def. (Cette �tape
   doit �tre r�p�t�e � chaque fois que les fichiers sources sont modifi�s). 


