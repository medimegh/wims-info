target=variable io if loop function vector1D vector2D file

#if defined TARGET_variable
#define TARGET variable
#define TARGET_FR variables
#endif

#if defined TARGET_io
#define TARGET io
#define TARGET_FR Entr�es-Sorties
#endif

#if defined TARGET_if
#define TARGET if
#define TARGET_FR conditionnelles
#endif

#if defined TARGET_loop
#define TARGET loop
#define TARGET_FR boucles
#endif

#if defined TARGET_function
#define TARGET function
#define TARGET_FR fonctions
#endif

#if defined TARGET_vector1D
#define TARGET vector1D
#define TARGET_FR tableaux
#endif

#if defined TARGET_vector2D
#define TARGET vector2D
#define TARGET_FR tableaux 2D
#endif

#if defined TARGET_file
#define TARGET file
#define TARGET_FR Fichiers et flux
#endif

\title{Compr�hension de programmes C++ (TARGET_FR)}
\description{Compr�hension de programmes C++ (TARGET_FR)}
\language{fr}
\niveau{U1}
\author{Nicolas M. Thi�ry}
\email{Nicolas.Thiery@u-psud.fr}
\format{html}

\integer{static=1}
\text{module_data=modules/devel/nthiery/test~coding~readingCppPrograms.fr/data/}
\text{programs=wims(lookup TARGET in data/static/index)}
\text{program=randomitem(\programs)}
\text{code=wims(record 0 of data/static/\program)}
\integer{height=wims(linecnt \code)+3}
\integer{heightpixels=10*\height}

\if{_input.cpp isin \program}{
  \text{answerstyle=text}
  \text{answer=wims(lines2words wims(record 0 of data/static/\program.answer))}
}{
\text{answerstyle=symtext}
\if{\static == 1}{
  \text{answer=wims(lines2words wims(record 0 of data/static/\program.answer))}
}{
  \text{answer=wims(lines2words wims(exec docker-compile-and-run \module_data/\program))}
}
}

\css{
<script type="text/javascript" src="scripts/js/edit_area/edit_area_full.js"></script>
<script type="text/javascript">
                editAreaLoader.init({
                        id: "wims_show"
                        ,start_highlight: true
                        ,allow_toggle: false
			,allow_resize: true
                        ,language: "fr"
                        ,syntax: "cpp"
			,min_height: 200
                        ,min_width: 300
                        ,is_editable:false
                        ,toolbar: ""
                        ,show_line_colors: false
                });
        </script>
}

\statement{
\if{_input.cpp isin \program}{
<p>Quel nombre entre 0 et 99 devrait saisir l'utilisateur pour que le programme C++ suivant affiche 42?</p>
}{
<p>Quel affichage exact produit le programme C++ suivant?</p>
<p>Les espaces, tabulations et sauts de lignes dans la r�ponse sont consid�r�s comme �quivalents.</p>
}

<textarea id="wims_show" cols="80" rows="\height" name="wims_show" readonly="readonly">\special{tabs2lines \code}</textarea>
}

\answer{}{\answer}{type=\answerstyle}
